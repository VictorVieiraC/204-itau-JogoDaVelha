package com.itau.jogaDaVelha.models;

public class Jogada {
	
	private int X;
	
	private int Y;

	public int getX() {
		return X;
	}

	public void setX(int X) {
		this.X = X;
	}

	public int getY() {
		return Y;
	}

	public void setY(int Y) {
		this.Y = Y;
	}
}
