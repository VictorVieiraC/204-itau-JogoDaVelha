package com.itau.jogaDaVelha.models;


public class JogoDaVelha {

	private boolean encerrado = true;
	private String[][] tabuleiro = new String[3][3];
	private int[] placar = new int[2];
	
	
	public boolean isEncerrado() {
		return encerrado;
	}
	
	public void setEncerrado(boolean encerrado) {
		this.encerrado = encerrado;
	}
	
	public String[][] getTabuleiro() {
		return tabuleiro;
	}
	
	public void setTabuleiro(String[][] tabuleiro) {
		this.tabuleiro = tabuleiro;
	}
	
	public void setTabuleiro(int x, int y, String valor) {
		this.tabuleiro[x][y] = valor;
	}
	
	public boolean validarPosicaoVazia(int x, int y) {
		if (this.tabuleiro[x][y] != "") {
			return false;
		}
		return true;
	}
	
	public void iniciarTabuleiro() {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				this.tabuleiro[i][j] = "";
			}
		}
		this.encerrado = false;
	}
	
	public boolean ganhou(String simbolo) {
		if (ganhouColuna(simbolo) || ganhouLinha(simbolo) || ganhouDiagonal(simbolo)) {
			return true;
		}
		return false;
	}
	
	public boolean ganhouColuna(String simbolo) {
		int contador = 0;
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				if(this.tabuleiro[i][j] == simbolo) {
					contador++;
				}
			}
			if (contador == 3) {
				return true;
			}
			contador = 0;
		}
		return false;
	}
	
	public boolean ganhouLinha(String simbolo) {
		int contador = 0;
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				if(this.tabuleiro[j][i] == simbolo) {
					contador++;
				}
			}
			if (contador == 3) {
				return true;
			}
			contador = 0;
		}
		return false;
	}
	
	public boolean ganhouDiagonal(String simbolo) {
		int contador = 0;
		for (int i = 0; i < 3; i++) {
			if(this.tabuleiro[i][i] == simbolo) {
				contador++;
			}
		}
		if (contador == 3) {
			return true;
		}
		if (this.tabuleiro[0][2] == simbolo && this.tabuleiro[1][1] == simbolo && this.tabuleiro[2][0] == simbolo) {
			return true;
		}
		return false;
	}
	
	public int[] getPlacar() {
		return placar;
	}

	public void setPlacar(int[] placar) {
		this.placar = placar;
	}
	
	public void iniciarPlacar() {
		this.placar[0] = 0;
		this.placar[1] = 0;
	}
	
	public void somarUmPlacar(String simbolo) {
		if (simbolo == "X") {
			this.placar[0]++;
		}
		if (simbolo == "O") {
			this.placar[1]++;
		}
	}
	
}
