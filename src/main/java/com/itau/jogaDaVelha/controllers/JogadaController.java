package com.itau.jogaDaVelha.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.itau.jogaDaVelha.models.Jogada;
import com.itau.jogaDaVelha.models.JogoDaVelha;

@RestController
public class JogadaController {
	
	private String jogadorAtivo = "X";
	
	private JogoDaVelha jogo = new JogoDaVelha();
	
	private int contadorJogada = 0;
	
	
	@RequestMapping(method=RequestMethod.POST, path="/JogoDaVelha/Iniciar")
	public ResponseEntity<?> iniciarCampeonato(){
		
		this.jogo.iniciarPlacar();
		
		return ResponseEntity.ok().body("O jogo começou aperte start...");
	}
	
	@RequestMapping(method=RequestMethod.POST, path="/JogoDaVelha/Iniciar/Rodada")
	public ResponseEntity<?> iniciarJogo() {
		
		if(jogo.isEncerrado()) {
			contadorJogada = 0;
			this.jogo.iniciarTabuleiro();
			return ResponseEntity.ok().body(jogo);	
		}
		return ResponseEntity.status(400).body("Não é possível iniciar uma nova rodada pois a atual ainda está em andamento.");
	}
	
	@RequestMapping(method=RequestMethod.POST, path="/JogoDaVelha/Iniciar/Rodada/Jogar")
	//ResponseEntity é usado para as mensagens de erro e sucesso
	public ResponseEntity<?> Jogada (@RequestBody Jogada jogada){
		
		if (jogo.isEncerrado()) {
			return ResponseEntity.status(400).body("Essa rodada já foi encerrada, inicie uma nova para continuar.");
		}
		
		if (jogo.validarPosicaoVazia(jogada.getX(), jogada.getY())) {
			jogo.setTabuleiro(jogada.getX(), jogada.getY(), jogadorAtivo);
			if (jogo.ganhou(jogadorAtivo)) {
				jogo.somarUmPlacar(jogadorAtivo);
				jogo.setEncerrado(true);
				return ResponseEntity.status(200).body("GANHOU!!!");
			}
			if (jogadorAtivo == "X") {
				jogadorAtivo = "O";
			}
			else if (jogadorAtivo == "O") {
				jogadorAtivo = "X";
			}
		}
		else { 
			return ResponseEntity.status(400).body("Essa casa não está disponível para jogar. Tente novamente.");
		}
		
		contadorJogada++;
		if (contadorJogada == 9) {
			return ResponseEntity.ok().body("Empatou! Jogue uma nova rodada!");
		}
		
		return ResponseEntity.ok().body(jogo);
		//return ResponseEntity.notFound().build();
	}
}
